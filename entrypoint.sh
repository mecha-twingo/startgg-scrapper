#!/usr/bin/env bash

set -e

case $NODE_ENV in
    "development")
        ./node_modules/.bin/nodemon src/index.js
    ;;
    
    "testing")
        node src/index.js
    ;;

    "production")
        node src/index.js
    ;;
esac

