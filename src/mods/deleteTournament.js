import { login, removeAdmins, unPublishEvent } from '../actions/index.js';

import config from '#config';
const { TOURNAMENT_PARAMS } = config;

import { getTournamentName } from "#helpers";

export default async (page) => {
    const tournamentsParams = TOURNAMENT_PARAMS.split(',')

    tournamentsParams.forEach((tournamentParams, index) => { if (!tournamentParams) delete tournamentsParams[index] })

    console.log(`Start deletion of ${tournamentsParams.length} tournament${tournamentsParams.length > 1 ? 's' : ''}`)
    if (!(await login(page))) {
        console.log('\t\t - Login Failed')
        return;
    }
    console.log('1/ Logged')

    let tournamentDeletedCounter = 0;
    for (const tournamentUrl of tournamentsParams) {
        let tournamentName = tournamentUrl;
        try {
            tournamentName = await getTournamentName(page, tournamentUrl)
            console.log(`\tStart deletion of tournament ${tournamentName}`)
            await unPublishEvent(page, tournamentName)
            console.log('\t\t2/ Unpublished')
            await removeAdmins(page, tournamentName)
            console.log('\t\t3/ Admins removed')
            console.log(`\tDeletion of tournament ${tournamentName}: Success`)
            tournamentDeletedCounter++
        } catch (e) {
            await page.screenshot({ path: 'previousError.jpg', fullPage: true })
            console.log(`Deletion of tournament ${tournamentName}: Failed${e === 404 ? '(not found)' : ''}`)
            console.log(`\tRaison: ${e.message}`);
        }
    }
    console.log(`=> Done! ${tournamentDeletedCounter} tournament${tournamentDeletedCounter > 1 ? 's' : ''} deleted`)
}