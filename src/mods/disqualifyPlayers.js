import { login, disqualifyParticipants } from "../actions/index.js";

import config from "#config";
const { TOURNAMENT_PARAMS } = config;

import { getTournamentName } from "#helpers";

export default async (page) => {
    const [tournamentUrl, ...participants] = TOURNAMENT_PARAMS.split(",");

    console.log(
      `Start disqualification of ${participants.length} participant${
        participants.length > 1 ? "s" : ""
      }`
    );

    if (!(await login(page))) {
      console.log("\t\t - Login Failed");
      return;
    }

    console.log("1/ Logged");
    
    const tournamentName = await getTournamentName(page, tournamentUrl)
    let disqualifyParticipantsNumber = 8;

    try {
        console.log(`\tStart disqualification for tournament ${tournamentName}`);
        disqualifyParticipantsNumber = await disqualifyParticipants(page, tournamentName, participants);
    } catch (e) {
        await page.screenshot({ path: 'previousError.jpg', fullPage: true })
        console.log(
          `Disqualification for tournament ${tournamentName}: Failed${
            e === 404 ? "(not found)" : ""
          }`
        );
        console.log(`\tRaison: ${e.message}`);
    }
    console.log(
      `=> Done! Disqualified participant${disqualifyParticipantsNumber > 1 ? 's' : ''} : ${disqualifyParticipantsNumber}`
    );
};
