import axios from 'axios';

import { convertEventForStartgg } from '#helpers';

import { editUrl, login, organizeEvent, publishEvent } from '../actions/index.js';

import config from '#config';
const { GSHEET_ID, GSHEET_URL, TOURNAMENT_PARAMS } = config;

export default async (page) => {
    const tournamentsParams = TOURNAMENT_PARAMS.split(',')

    tournamentsParams.forEach((tournamentParams, index) => { if (!tournamentParams) delete tournamentsParams[index] })

    console.log(`Start creation of ${tournamentsParams.length} tournament${tournamentsParams.length > 1 ? 's' : ''}`)

    console.log('\t1/ Login')
    
    if (!(await login(page))) {
        console.log('\t\t - Login Failed')
        return;
    }

    const events = await axios.get(`${GSHEET_URL}${GSHEET_ID}`);
    let tournamentCreatedCounter = 0;

    for (const tournamentIndex of await tournamentsParams) {
        try {
            const event = await events.data.find(e => e.id === tournamentIndex)

            if (!event) throw new Error(`No tournament find for index ${tournamentIndex}`)
            if (!event.creationAuto) throw new Error(`Automatic creation is not activated on tournament with id ${event.id}`);

            const eventFormatted = convertEventForStartgg(event);

            console.log(`\tStart generation of tournament with index ${tournamentIndex}: ${eventFormatted.title}`)
            console.log('\t\t2/ Creation')
            await organizeEvent(page, eventFormatted);
            console.log('\t\t3/ Publish')
            await publishEvent(page);
            console.log('\t\t4/ Share')
            await editUrl(page, eventFormatted)
            console.log('\t\tFinished !')
            console.log(`\tCreation of tournament ${tournamentIndex}: Success`)
            tournamentCreatedCounter++;
        } catch (e) {
            await page.screenshot({ path: 'previousError.jpg', fullPage: true })
            console.log(`\tCreation of tournament ${tournamentIndex}: Failed`)
            console.log(`\tRaison: ${e.message}`);
        }
    }
    console.log(`=> Done! ${tournamentCreatedCounter} tournament${tournamentCreatedCounter > 1 ? 's' : ''} created`)
}