import { DEFAULT_INTERCEPT_RESOLUTION_PRIORITY } from 'puppeteer'
import puppeteer from 'puppeteer-extra';
import stealthPlugin from 'puppeteer-extra-plugin-stealth'
import recaptchaPlugin from 'puppeteer-extra-plugin-recaptcha'
import adblockerPlugin from 'puppeteer-extra-plugin-adblocker'
import userAgent from 'user-agents';

import { createTournament, deleteTournament, disqualifyPlayers } from './mods/index.js';

import config from '#config';
const { HEADLESS, CAPTCHA_TOKEN, MOD } = config;

(async () => {
    const [width, height] = [1280, 720]

    puppeteer.use(stealthPlugin())
    puppeteer.use(adblockerPlugin({
        interceptResolutionPriority: DEFAULT_INTERCEPT_RESOLUTION_PRIORITY,
        blockTrackers: true
    }))
    puppeteer.use(recaptchaPlugin({
        provider: {
            id: '2captcha',
            token: CAPTCHA_TOKEN
        },
        visualFeedback: true
    }))



    const browser = await puppeteer.launch(HEADLESS ?
        {
            headless: 'new',
            executablePath: '/usr/bin/google-chrome-stable',
            args: [' --no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage', `--window-size=${width},${height}`]
        } : {
            args: [
                '--disable-setuid-sandbox',
                `--window-size=${width},${height}`,
            ],
            headless: false,
        });

    const page = await browser.newPage();

    await page.setDefaultTimeout(0);

    await page.emulateTimezone('Europe/Paris')
    await page.setViewport({ width, height })
    await page.setUserAgent(userAgent.random().toString());

    switch (MOD) {
        case 'creation':
            await createTournament(page)
            break;
        case 'deletion':
            await deleteTournament(page)
            break;
        case 'disqualification':
            await disqualifyPlayers(page)
            break;        
        default:
            console.log(`Unknown mod: ${MOD}`)
    }

    await browser.close();
    
})();
