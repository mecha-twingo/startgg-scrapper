import publishSection from './publishSection/publishSection.js';

export default async (page) => {
    console.log('\t\t\t- Publish Homepage')
    await publishSection(page, 'Homepage');

    console.log('\t\t\t- Publish Events')
    await publishSection(page, 'Events');

    console.log('\t\t\t- Publish Registrations')
    await publishSection(page, 'Registration');
};
