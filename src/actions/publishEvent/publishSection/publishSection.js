import config from '#config';
const { NODE_ENV } = config;

export default async (page, title) => {
    const formWindow = `//form[.//span[contains(., "${title}")]]`

    await page.locator(`xpath///button[contains(., "Publish") and ../div//a[contains(., "${title}")]][./span]`).click();
    await page.locator(`xpath/${formWindow}//section[contains(., "Public")]`).click();

    if (title === 'Homepage') {
        await page.locator(`xpath/${formWindow}//div[@role="button"]`).click();
        await page.locator(`xpath///li[contains(., "${NODE_ENV === 'production' ? 'Discoverable' : 'Link only'}")]`).click();
    }

    await page.locator('[type=submit]').click();
    await page.waitForSelector(`xpath/${formWindow}`, { hidden: true })
}
