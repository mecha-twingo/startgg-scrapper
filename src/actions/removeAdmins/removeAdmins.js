import { getUserLink } from '#helpers';
import config from '#config';

const { BOT_NAME } = config;

export default async (page, tournamentName) => {
    await page.goto(`https://www.start.gg/admin/tournament/${tournamentName}/permissions`, { waitUntil: 'networkidle0' });

    const deleteAdmins = async () => {
        const adminRows = await page.$$(`xpath///tr[.//i[contains(@class, "fa-trash")] and not(.//span[contains(., "${BOT_NAME}")])]`)
        
        if (adminRows.length > 0) {
            const adminRow = adminRows.shift();

            const trashButton = await adminRow.waitForSelector('xpath/.//i[contains(@class, "fa-trash")]')
            await trashButton.click()
            await page.locator('[type=submit]').click()

            await page.waitForSelector(`xpath///a[contains(@href, "${await getUserLink(page, await adminRow.$$('xpath/.//a'))}")]`, { hidden: true })
            await deleteAdmins()
        }
    }
    await deleteAdmins();

    await page.locator(`xpath///button[(i[contains(@class, "fa-trash")]) and (../../..//span[contains(., "${BOT_NAME}")])]`).click()

    await page.locator('[type=submit]').click()
    await page.waitForSelector(('xpath///div[.//h2[contains(., "Remove Admin Permission")]]'), { hidden: true })
};