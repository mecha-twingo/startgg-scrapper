export default async (page, event) => {
    await page.goto((await page.url()).replace('dashboard', 'settings'));
    await page.locator('[name=shortSlug]').fill(event.shortLink)
    await page.locator('[type=submit]:not([disabled])').click();

    await page.waitForResponse('https://www.start.gg/api/-/gql');
    await page.waitForResponse('https://www.start.gg/api/-/gql');
};