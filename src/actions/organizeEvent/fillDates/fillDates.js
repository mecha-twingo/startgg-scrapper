import moment from 'moment';

import config from '#config';
const { HEADLESS } = config;

export default async (page, event) => {
    const startDate = (`${moment().startOf('isoWeek').add(event.day.toLowerCase() === 'sunday' ? 2 : 1, 'week').day(event.day).format('L')} ${event.startAt}`)
    const endDate = (`${moment().startOf('isoWeek').add(event.day.toLowerCase() === 'sunday' ? 2 : 1, 'week').day(event.day).format('L')} ${event.endAt}`)

    if (HEADLESS) { 
         await page.locator('input[name=startAt]').click();

        await page.locator('[role=presentation] [data-testid=PenIcon]').click();
        await page.locator('[role=presentation] [name=startAt]').fill(startDate);

        await page.locator('xpath///button[contains(., "OK")]').click()

        await page.waitForSelector('[role=presentation] .MuiDateTimePickerToolbar-timeContainer', { hidden: true })

        await page.locator('input[name=endAt]').click();

        await page.locator('[role=presentation] [data-testid=PenIcon]').click();
        await page.locator('[role=presentation] [name=endAt]').fill(endDate);

        await page.locator('xpath///button[contains(., "OK")]').click()

        await page.waitForSelector('[role=presentation] .MuiDateTimePickerToolbar-timeContainer', { hidden: true })
    } else {
        await page.locator('input[name=startAt]').fill(startDate);
        await page.locator('input[name=endAt]').fill(endDate);
    }
};