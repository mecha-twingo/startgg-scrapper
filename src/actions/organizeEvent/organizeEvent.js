import { copyTournamentSettings, fillDates, handleSubmit } from './index.js';
import { captchaHandler } from "../../helpers/index.js";

import config from '#config';
const { DISCORD_INVITE } = config;

export default async (page, event) => {
    console.log('\t\t\t- Go to creation Page')
    await page.goto('https://www.start.gg/create/tournament', { waitUntil: 'networkidle2' });
    
    console.log('\t\t\t- Get previous tournament')
    await page.locator('[name=name]').fill(await copyTournamentSettings(page, event));

    console.log('\t\t\t- Fill contacts')
    await page.locator('#primaryContactType').click();
    await page.locator('[data-value=discord]').click()
    await page.locator('[name=primaryContact]').fill(`https://discord.gg/${DISCORD_INVITE}`);

    console.log('\t\t\t- Fill dates')
    await fillDates(page, event);

    console.log('\t\t\t- Fill captcha')
    await captchaHandler(page)

    console.log('\t\t\t- Submit')
    await handleSubmit(page)
}