import getTitles from './getTitles.js';

export default async (page, event) => {
    await page.locator('xpath///button[contains(., "Copy Tournament Settings")]').click()

    await page.locator('[role=combobox]').fill(event.title)
    
    const previousTournamentsXPath = '//div[contains(@class, "Select-option")]'
    const previousTournamentsComponent = await page.waitForSelector(`xpath///div[contains(@class, "Select-menu-outer")][${previousTournamentsXPath}]`);
    const previousTournaments = await previousTournamentsComponent.$$(`xpath/${previousTournamentsXPath}`)

    const { oldTitle, newTitle } = await getTitles(event.title, previousTournaments);

    await page.locator('[role=combobox]').fill(oldTitle)
    await page.waitForSelector('xpath///div[contains(., "Loading...")]', { hidden: true })
    await page.keyboard.press('Enter');

    return newTitle;
};