import config from '#config';

const { BOT_NAME } = config;

export default async (eventName, previousTournaments) => {
    const previousEdition = await previousTournaments.reduce(async (res, t) => {
        const [title, metaData] = (await t.evaluate(c => c.textContent)).split('#');
        return (eventName === title.slice(0, -1) && Number(metaData.split(' ')[0]) > await res) ? metaData.split(' ')[0] : await res;
    }, 0)

    if (previousEdition === 0) throw new Error(`${BOT_NAME} can't initialize a tournament`)
    
    return { oldTitle: `${eventName} #${Number(previousEdition)}`, newTitle: `${eventName} #${Number(previousEdition) + 1}` };
}
