export default async (page) => {
    await page.locator('[type="submit"]').click()
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' })
}