import copyTournamentSettings from './copyTournamentSettings/copyTournamentSettings.js';
import fillDates from './fillDates/fillDates.js';
import handleSubmit from './handleSubmit/handleSubmit.js';

export { copyTournamentSettings, fillDates, handleSubmit };