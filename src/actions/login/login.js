import config from '#config'

import loginWithCaptcha from './loginWithCaptcha.js';

const { STARTGG_EMAIL, STARTGG_PASSWORD } = config;

export default async (page) => {
  console.log('\t\t- Go to login page')
  await page.goto('https://www.start.gg/', { waitUntil: 'domcontentloaded' });

  console.log('\t\t- Handle cookies')
  await page.locator('.cky-btn-accept').click();
  await page.$eval('[name=loginOrRegister]', b => b.click());

  console.log('\t\t- Fill login informations')
  await page.locator('[name=loginEmail]').fill(STARTGG_EMAIL);
  await page.locator('[name=loginPassword]').fill(STARTGG_PASSWORD);

  console.log('\t\t- Submit')
  await page.$eval('[name=loginSubmit]', b => b.click());

  console.log('\t\t- Wait a response')
  const res = await page.waitForResponse('https://www.start.gg/api/-/rest/user/login')

  console.log('\t\t- Treat response status')
  if (res.status() !== 200) {
    console.log('\t\t- Need captcha to login')
    return loginWithCaptcha(page);
  }
  
  console.log('\t\t- Login success')
  return true;
};