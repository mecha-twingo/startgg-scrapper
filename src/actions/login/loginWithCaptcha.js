import { captchaHandler } from "../../helpers/index.js";

export default async (page) => {
  await captchaHandler(page)
  await page.$eval('[name=loginSubmit]', b => b.click());
  const res = await page.waitForResponse('https://www.start.gg/api/-/rest/user/login')
  return res.status() === 200
};