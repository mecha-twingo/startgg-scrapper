import { disqualifyParticipants } from '../index.js';

export default async (page, tournamentName, participants, pageNumber = 1) => {
    await page.goto(`https://www.start.gg/admin/tournament/${tournamentName}/attendees?page=${pageNumber}`, { waitUntil: 'networkidle0' });

    const attendeesElement = await page.waitForSelector('xpath///div[contains(@class, "text-right")]/span[contains(@aria-live, "polite")]')
    const attendees = await page.evaluate(element => element.textContent, attendeesElement)

    if(attendees === 'No Results') {
        return 0;
    }

    let disqualifiedParticipantsNumber = 0;

    const tryToDq = async () => {
        if (participants.length === 0) {
            return
        }
        const selector = 'xpath///table[contains(@role, "table")]//tbody/tr[.//a//span[contains(@data-test,"gamertag")]]'
        await page.waitForSelector(selector)
        const rows = await page.$$(selector)
        
        for (const row of rows) {
            const getLink = await row.waitForSelector('xpath/.//a[.//span[contains(@data-test,"gamertag")]]')
            const participantLogoUrl = await page.evaluate(anchor => anchor.getAttribute('href'), getLink)
            const participantId = participantLogoUrl.split('/')[participantLogoUrl.split('/').length-1]

            if(participants.includes(participantId)) {
                await row.scrollIntoView()
                const settingsButton = await row.waitForSelector(`xpath/.//button[./*[contains(@data-testid, "MoreVertIcon")]]`, { waitUntil: 'networkidle0' })
                await settingsButton.click()

                await page.locator(`xpath///div[contains(@role, "presentation")]//div[not(contains(@style,"visibility: hidden"))]//li[contains(., "Remove Attendee")]`).click()
                await page.locator(`xpath///form//button[contains(., "Remove")]`).click()
                await page.waitForSelector('xpath///div/form[not(contains(@class, "filter-form"))]', { hidden: true })
                participants.splice(participants.findIndex(e => e === participantId), 1)
                disqualifiedParticipantsNumber+=1
                console.log(`\tDisqualification of participant ${participantId}: Success`)
                await tryToDq()
                break
            }
        }
    }
    await tryToDq()

    const [, current, expected ] = attendees.match(/^Displaying [0-9]+ - ([0-9]+) of ([0-9]+) attendees$/)
    if ( current !== expected && participants.length > 0) {
        disqualifiedParticipantsNumber += await disqualifyParticipants(page, tournamentName, participants, pageNumber+1)
    }

    return disqualifiedParticipantsNumber;
};
