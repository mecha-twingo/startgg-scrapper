export default async (page, tournamentName) => {
    await page.goto(`https://www.start.gg/admin/tournament/${tournamentName}/dashboard`, { waitUntil: 'networkidle0' });

    await page.waitForSelector('xpath///button[(contains(., "Unpublish") or contains(., "Publish")) and ../div//a[contains(., "Homepage")]][./span]')

    const [ unpublishButton ] = await page.$$('xpath///button[contains(., "Unpublish") and ../div//a[contains(., "Homepage")]][./span]')

    if (unpublishButton) {
        await unpublishButton.click();

        const formWindow = await page.waitForSelector('xpath///form[.//span[contains(., "Homepage")]]')

        const adminSelect = await page.waitForSelector('xpath///form[.//span[contains(., "Homepage")]]//div[contains(., "Admins Only")]')
        await adminSelect.click()
        
        await page.locator('[type=submit]').click();
        await formWindow.waitForSelector(('*'), { hidden: true })
    }
};
