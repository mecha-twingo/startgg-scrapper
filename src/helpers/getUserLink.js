export default async (page, userLinks) => await userLinks.reduce(async (res, cur) => {
    const playerLink = await page.evaluate(el => el.getAttribute('href'), cur)
    return playerLink.includes('https://www.start.gg/user/') ? playerLink : res
}, '');