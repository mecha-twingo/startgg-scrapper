import moment from 'moment';

const convertDays = {
    'Lundi': 'Monday',
    'Mardi': 'Tuesday',
    'Mercredi': 'Wednesday',
    'Jeudi': 'Thursday',
    'Vendredi': 'Friday',
    'Samedi': 'Saturday',
    'Dimanche': 'Sunday'
}

const handleDate = (date) => {
    const [h, t] = moment(date.replace('h', ':'), 'hh:mm').
        format('LT').
        toLowerCase().
        split(':');
    return h.length === 1 ? `0${h}:${t}` : `${h}:${t}`;
}
const isBefore = (startDate, endDate) => moment(startDate, 'hh:mm').isBefore(moment(endDate, 'hh:mm'))

const errorOnField = (key) => { throw new Error(key) }

export default (event) => {
    try {
        return Object.entries(event).reduce((res, [key, value]) => ({
            ...res,
            ...(key === 'day') && (value && convertDays[value] ? { day: convertDays[value] } : errorOnField(key)),
            ...(key === 'startAt') && (value ?  { startAt: handleDate(value) } : errorOnField(key)),
            ...(key === 'endAt') && (value && isBefore(res.startAt, handleDate(value)) ?  { endAt: handleDate(value) } : errorOnField(key)),
            ...(key === 'shortLink') && (value && (value.split('https://www.start.gg/').length === 2) ? { shortLink: value.split('https://www.start.gg/')[1] } : errorOnField(key))
        }), event)
    } catch (err) {

        throw Error(`Erreur dans le googleSheet: ${err.message}`)
    }
};