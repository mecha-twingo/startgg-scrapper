import axios from 'axios'

const regex = /^https:\/\/(?:www.)?start.gg\/(?:(?:admin\/)?tournament\/)?((?:[a-z]|[0-9]|-)+)(?:\/.*)?$/

const extractTournamentName = (link) => (link.match(regex)[1].trim())

export default async (page, url) => {
    if (!regex.test(url)) throw Error("Url de tournoi non valide");

    const cookies = (await page.cookies()).reduce((acc, cur) => `${acc}${cur.name}=${cur.value};`,'')
    const { request } = await axios.get(url, { headers: { Cookie: await cookies }}).catch(() => { throw 404 })

    return extractTournamentName(request.res.responseUrl)
}