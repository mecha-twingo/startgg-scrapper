import config from '#config';
const { HEADLESS } = config;

export default async (page) => {
    const iframe = await page.waitForSelector('iframe[src*="recaptcha/"]');

    if (HEADLESS) {
        const { solved } = await page.solveRecaptchas();

        if(solved.length === 0 || !solved[0].isSolved)
            throw new Error('Captcha not solved')

    } else {
        const frame = await iframe.contentFrame();
        await frame.waitForSelector('#recaptcha-anchor[aria-checked=true]')
    }
};

