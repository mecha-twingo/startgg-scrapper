FROM node:22

ARG NODE_ARG="development"
ENV NODE_ENV=$NODE_ARG
ENV DEBUG=puppeteer-extra-plugin:recaptcha
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

RUN apt-get update && apt-get install gnupg wget -y && \
    wget --quiet --output-document=- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor > /etc/apt/trusted.gpg.d/google-archive.gpg && \
    sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' && \
    apt-get update && \
    apt-get install apt-utils -y --no-install-recommends && \
    apt-get install google-chrome-stable -y --no-install-recommends  && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY ./src /app/src
COPY ./entrypoint.sh /app/
COPY ./package*.json /app/
COPY ./config /app/config

RUN if [ "$NODE_ENV" != "development" ]; then npm ci --omit=dev ; fi

ENTRYPOINT ["sh", "entrypoint.sh"]