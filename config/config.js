import { createRequire } from 'node:module';
import 'dotenv/config';

const require = createRequire(import.meta.url);
const config = require('./config.json');

const { NODE_ENV = 'development', HEADLESS = 'true', CAPTCHA_TOKEN, TOURNAMENT_PARAMS, STARTGG_EMAIL, STARTGG_PASSWORD, MOD } = process.env;
const mandatoryFields = { CAPTCHA_TOKEN, STARTGG_EMAIL, STARTGG_PASSWORD, MOD, TOURNAMENT_PARAMS }

Object.entries(mandatoryFields).map(([key, value]) => { if (!value) { throw new Error(`${key} is missing`) } })

export default {
    ...config.default,
    ...(NODE_ENV === 'development' && config.development),
    ...(NODE_ENV === 'testing' && config.testing),
    ...(NODE_ENV === 'production' && config.production),
    HEADLESS: HEADLESS === 'true',
    MOD,
    CAPTCHA_TOKEN,
    TOURNAMENT_PARAMS,
    STARTGG_EMAIL,
    STARTGG_PASSWORD,
    NODE_ENV
}

